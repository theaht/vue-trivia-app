# Trivia Game

Application made by: Camilla Stokbro Felin and Thea Thodesen

Click [here](https://tc-trivia-game.herokuapp.com/) to access the app on Heroku.


This app fetches questions from the [Open Trivia Database API](https://opentdb.com/api_config.php)

## Start page
In the start page the users can select how many questions they want.
They can also select difficulty and category. 

When the user clicks 'Play', the user will be directed to the game page. 

## Game page
In this page the user will be displayed with one question at the time. The user will not be allowed to
skip a question. 

## Results page

In the results page, all the answered questions will be displayed with the correct answer, 
the user's answer and if it was correct. 
A score will also be displayed, each question is worth 10 points. 

At the bottom of the page there is a button to play again, this will direct the user to the start page. 




## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
