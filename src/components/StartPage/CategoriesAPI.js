
/**
 * This method fetches all the categories from the API.
 * */
export const CategoriesAPI  = {
    fetchCategories(){
         return fetch(`https://opentdb.com/api_category.php`)
             .then(response => response.json());

    }

}
