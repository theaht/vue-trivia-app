
/**
 * This method fetches questions from the API, based on the input of the user on the start page.
 * */
export const QuestionsAPI  = {
    fetchQuestions(amount, category, difficulty){
        return fetch(`https://opentdb.com/api.php?amount=${amount}&category=${category}&difficulty=${difficulty}`)
            .then(response => response.json());

    }

}