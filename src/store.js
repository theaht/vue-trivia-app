import {CategoriesAPI} from "@/components/StartPage/CategoriesAPI"
import {QuestionsAPI} from "@/components/StartPage/QuestionsAPI";

export const store = {
    /**
     * shared states between store and components
     * */
    state: {
        categories: [],
        questions: [],
        results: []
    },
    /**
     * This method calls the function fetchCategories and calls the function setCategories with the categories as parameter
     * */
    getCategories() {
        return CategoriesAPI.fetchCategories().then(categories => {
            this.setCategories(categories)

        })
    },

    /**
     * This set the state categories.
     * */
    setCategories(categories = []) {
        this.state.categories = categories.trivia_categories;
        console.log(this.state.categories)
    },
    /**
     * This method calls the function fetchQuestions and calls the function setQuestions with the questions as parameter
     * */
    getQuestions(amount, category, difficulty){
        return QuestionsAPI.fetchQuestions(amount, category,difficulty).then(questions => {
            this.setQuestions(questions);
        })
    },

    /**
     * This sets the state questions
     * */
    setQuestions(questions = []){
        this.state.questions = questions.results;
        console.log(questions)
    }
}
