import VueRouter from 'vue-router';
import StartPage from "@/components/StartPage/StartPage";
import TriviaGamePage from "@/components/GamePage/TriviaGamePage";
import ResultPage from "@/components/ResultPage/ResultPage";
//import App from './App'

const routes = [
    {
        path: '/start-page',
        name: 'StartPage',
        component: StartPage,
        alias: '/'

    },
    {
       path: '/trivia-game',
       name: "TriviaGamePage",
       component: TriviaGamePage
    },
    {
        path: '/result-page',
        name: "ResultPage",
        component: ResultPage

    }
]

const router = new VueRouter({routes});

export default router;